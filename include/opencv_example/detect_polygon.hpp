#ifndef OPENCVEXAMPLE_HPP
#define OPENCVEXAMPLE_HPP

#include <ros/ros.h>
#include <cv_bridge/cv_bridge.h>
#include <opencv2/opencv.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/Image.h>

#include <geometry_msgs/PoseStamped.h>

class DetectPolygon
{
  public:

    // Constructor:
    DetectPolygon();
    ~DetectPolygon();

    // Image Callback:

    void image_Callback(const sensor_msgs::Image::ConstPtr& msg);

    // Timer Callback:
    void spin(const ros::TimerEvent& event);

    // Variables:

    //void on_trackbar(int, void*);

    // Topics:

    std::string image_topic;
    std::string disk_topic;
    std::string robot1_topic;
    std::string robot2_topic;
    std::string image_on_disk_path;

    // Subscribers:

    ros::Subscriber image_sub;

    // Publishers:

    ros::Publisher pose_pub;

    // Ros timer:

    ros::Timer spin_timer;


    // Images:

    cv::Mat image;


    cv::Mat thresholdg1, thresholdg2, thresholdgray;

    cv::Mat dst;

    cv::Mat gray;
    cv::Mat threshold;
    cv::Mat canny;



    // Contours and polygons:

    cv::vector<cv::vector<cv::Point> > contours;
    cv::vector<cv::Vec4i> hierarchy;
    cv::vector<cv::Point> approx;

    // Pose:

    geometry_msgs::PoseStamped disk_pose;

    // Other:
    // Variables:

    int gray_min, gray_max;
    int canny_min, canny_max;

    bool use_web_cam;


};

#endif // OPENCVEXAMPLE_HPP
