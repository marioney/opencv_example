#ifndef OPENCVEXAMPLE_HPP
#define OPENCVEXAMPLE_HPP

#include <ros/ros.h>
#include <cv_bridge/cv_bridge.h>
#include <opencv2/opencv.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/Image.h>

#include <geometry_msgs/PoseStamped.h>

class ColorFilter
{
  public:

    // Constructor:
    ColorFilter();
    ~ColorFilter();

    // Image Callback:

    void image_Callback(const sensor_msgs::Image::ConstPtr& msg);

    // Timer Callback:
    void spin(const ros::TimerEvent& event);



    // Variables:

    // Topics:

    std::string image_topic;
    std::string output_image_topic;
    std::string image_on_disk_path;

    // Subscribers:

    ros::Subscriber image_sub;

    // Publishers:

    ros::Publisher image_pub;

    // Ros timer:

    ros::Timer spin_timer;


    // Images:

    cv::Mat image;

    cv::Mat hsv_image;
    cv::Mat image_out;

    cv::Mat channel[3];

    cv::Mat thresholdh1, thresholdh2, thresholds1, thresholds2, thresholdv1, thresholdv2, thresholdhsv;

    cv::Mat dst;

    cv_bridge::CvImage image_msg;

    // Contours and polygons:

    cv::vector<cv::vector<cv::Point> > contours;
    cv::vector<cv::Vec4i> hierarchy;
    cv::vector<cv::Point> approx;



    // Other:
    // Variables:

    int hue_min, hue_max, sat_min, sat_max, val_min, val_max;

    bool use_web_cam;



};

#endif // OPENCVEXAMPLE_HPP
