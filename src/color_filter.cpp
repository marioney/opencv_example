#include <opencv_example/color_filter.hpp>
// Functions:

void hue_min_trackbar(int, void*){}

void hue_max_trackbar(int, void*){}

void sat_min_trackbar(int, void*){}

void sat_max_trackbar(int, void*){}

void val_min_trackbar(int, void*){}

void val_max_trackbar(int, void*){}

ColorFilter::ColorFilter()
{
  // NodeHandle:

    ros::NodeHandle n("~");

    // Params:

    double timer_rate;
    n.param("image_topic", image_topic, std::string("/usb_cam/image_raw"));
    n.param("output_image_topic", output_image_topic, std::string("/color_filter/image"));
    n.param("image_path", image_on_disk_path, std::string("(find opencv_example)/images/polygons.jpg"));
    n.param("use_web_cam", use_web_cam, true);
    n.param("timer_rate", timer_rate, 1.0);


    // Subscribers:

    if(use_web_cam)
    {
      image_sub = n.subscribe(image_topic, 1, &ColorFilter::image_Callback, this);
    }


    // Color filter:

    hue_min = 0;
    hue_max = 255;
    sat_min = 0;
    sat_max = 255;
    val_min = 0;
    val_max = 255;

    cv::namedWindow("TrackBars", cv::WND_PROP_ASPECT_RATIO);

    char TrackbarName[50];

    sprintf(TrackbarName, "Hue - Min:         ");
    cv::createTrackbar(TrackbarName, "TrackBars", &hue_min, 255, hue_min_trackbar);
    hue_min_trackbar(hue_min, 0);

    sprintf(TrackbarName, "Hue - Max:         ");
    cv::createTrackbar(TrackbarName, "TrackBars", &hue_max, 255, hue_max_trackbar);
    hue_max_trackbar(hue_max, 0);

    sprintf(TrackbarName, "Saturation - Min:");
    cv::createTrackbar(TrackbarName, "TrackBars", &sat_min, 255, sat_min_trackbar);
    sat_min_trackbar(sat_min, 0);

    sprintf(TrackbarName, "Saturation - Max:");
    cv::createTrackbar(TrackbarName, "TrackBars", &sat_max, 255, sat_max_trackbar);
    sat_max_trackbar(sat_max, 0);

    sprintf(TrackbarName, "Value - Min:       ");
    cv::createTrackbar(TrackbarName, "TrackBars", &val_min, 255, val_min_trackbar);
    val_min_trackbar(val_min, 0);

    sprintf(TrackbarName, "Value - Max:       ");
    cv::createTrackbar(TrackbarName, "TrackBars", &val_max, 255, val_max_trackbar);
    val_max_trackbar(val_max, 0);

    image = cv::imread(image_on_disk_path);

    image_pub = n.advertise<sensor_msgs::Image>(output_image_topic, 1);
    // create timer
    spin_timer = n.createTimer(ros::Duration(timer_rate), &ColorFilter::spin, this);

    ROS_INFO("[OpenCvExample] -  Node initialization OK");

}

ColorFilter::~ColorFilter()
{
  cv::destroyAllWindows();
}
  // Image Callback:
void ColorFilter::image_Callback(const sensor_msgs::Image::ConstPtr &msg)
{


    // Receive image:

    cv_bridge::CvImagePtr cv_ptr;

    try
    {
      cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
      image = cv_ptr->image;
    }
    catch (cv_bridge::Exception& e)
    {
      ROS_ERROR("cv_bridge exception: %s", e.what());
      return;
    }

    return;

}

void ColorFilter::spin(const ros::TimerEvent &event)
{
  //ROS_INFO("h: (%d, %d) - s: (%d, %d) - v: (%d, %d)", hue_min, hue_max, sat_min, sat_max, val_min, val_max);

  if(!use_web_cam)
  {
    image = cv::imread(image_on_disk_path);
  }

  dst = cv::Mat();
  thresholdh1 = cv::Mat();
  thresholdh2 = cv::Mat();
  thresholds1 = cv::Mat();
  thresholds2 = cv::Mat();
  thresholdv1 = cv::Mat();
  thresholdv2 = cv::Mat();
  thresholdhsv = cv::Mat();

  cvtColor(image, hsv_image, CV_BGR2HSV);

  cv::split(hsv_image, channel);

  cv::threshold(channel[0], thresholdh1, hue_min, 255, 0);
  cv::threshold(channel[0], thresholdh2, hue_max, 255, 1);
  cv::threshold(channel[1], thresholds1, sat_min, 255, 0);
  cv::threshold(channel[1], thresholds2, sat_max, 255, 1);
  cv::threshold(channel[2], thresholdv1, val_min, 255, 0);
  cv::threshold(channel[2], thresholdv2, val_max, 255, 1);

  thresholdhsv = thresholdh1 & thresholdh2 & thresholds1 & thresholds2 & thresholdv1 & thresholdv2;


  //
  image.copyTo(dst, thresholdhsv);
  cvtColor(dst, image_out, CV_HSV2BGR);
  image_msg.header.seq ++;
  image_msg.header.stamp = ros::Time::now();
  image_msg.header.frame_id = "video";
  image_msg.image = dst;
  image_msg.encoding = sensor_msgs::image_encodings::BGR8;

  image_pub.publish(image_msg.toImageMsg());

  cv::imshow("Color_filter", dst);
  cv::waitKey(3);


}

