#include <opencv_example/detect_polygon.hpp>
// Functions:

void gray_min_trackbar(int, void*){}

void gray_max_trackbar(int, void*){}

void canny_trackbar(int, void*){}


DetectPolygon::DetectPolygon()
{
  // NodeHandle:

    ros::NodeHandle n("~");

    // Params:

    double timer_rate;
    n.param("image_topic", image_topic, std::string("/usb_cam/image_raw"));
    n.param("image_path", image_on_disk_path, std::string("(find opencv_example)/images/polygons.jpg"));
    n.param("use_web_cam", use_web_cam, true);
    n.param("timer_rate", timer_rate, 1.0);


    // Subscribers:

    if(use_web_cam)
    {
      image_sub = n.subscribe(image_topic, 1, &DetectPolygon::image_Callback, this);
    }



    // Color filter:

    gray_min = 0;
    gray_max = 255;
    canny_min= 0;



    cv::namedWindow("TrackBars", cv::WND_PROP_ASPECT_RATIO);

    char TrackbarName[50];

    sprintf(TrackbarName, "Gray - Min:         ");
    cv::createTrackbar(TrackbarName, "TrackBars", &gray_min, 255, gray_min_trackbar);
    gray_min_trackbar(gray_min, 0);

    sprintf(TrackbarName, "Gray - Max:         ");
    cv::createTrackbar(TrackbarName, "TrackBars", &gray_max, 255, gray_max_trackbar);
    gray_max_trackbar(gray_max, 0);

    sprintf(TrackbarName, "Canny :         ");
    cv::createTrackbar(TrackbarName, "TrackBars", &canny_min, 255, canny_trackbar);
    canny_trackbar(canny_min, 0);



    // create timer
    spin_timer = n.createTimer(ros::Duration(timer_rate), &DetectPolygon::spin, this);

    ROS_INFO("[OpenCvExample] -  Node initialization OK");

}

DetectPolygon::~DetectPolygon()
{
  cv::destroyAllWindows();
}
  // Image Callback:
void DetectPolygon::image_Callback(const sensor_msgs::Image::ConstPtr &msg)
{


    // Receive image:

    cv_bridge::CvImagePtr cv_ptr;

    try
    {
      cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
      image = cv_ptr->image;
    }
    catch (cv_bridge::Exception& e)
    {
      ROS_ERROR("cv_bridge exception: %s", e.what());
      return;
    }

    return;

}

void DetectPolygon::spin(const ros::TimerEvent &event)
{
  //ROS_INFO("h: (%d, %d) - s: (%d, %d) - v: (%d, %d)", hue_min, hue_max, sat_min, sat_max, val_min, val_max);

  if(!use_web_cam)
  {
    image = cv::imread(image_on_disk_path);
  }

  dst = cv::Mat();
  thresholdgray = cv::Mat();
  thresholdg1 = cv::Mat();
  thresholdg2 = cv::Mat();
  cv::Mat edges;

  cvtColor(image, gray, CV_BGR2GRAY);
 //  ROS_INFO("conversion ok");

  cv::threshold(gray, thresholdg1, gray_min, 255, 0);
  cv::threshold(gray, thresholdg2, gray_max, 255, 1);
  thresholdgray = thresholdg1 & thresholdg2;

 // cv::threshold(gray, threshold, 230, 255, 0);
 // ROS_INFO("threshold ok");

  cv::blur(thresholdgray, edges, cv::Size(3,3));
    int niters = 2;
    cv::dilate(edges, edges, cv::Mat(), cv::Point(-1,-1), niters);
    cv::erode(edges, edges, cv::Mat(), cv::Point(-1,-1), niters);
    cv::dilate(edges, edges, cv::Mat(), cv::Point(-1,-1), niters);

 // ROS_INFO("blur ok");
  cv::Canny(edges, canny, canny_min, canny_min+20.0, 3);
 // ROS_INFO("canny ok");

  cv::findContours(canny, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, cv::Point(0, 0));

  image.copyTo(dst);

  for(int i = 0; i < contours.size(); i++)
  {
      approxPolyDP(contours[i], approx, 7, true);

      switch(approx.size())
      {
          case 3:
          {
              // Triángulo:

              cv::line(dst, approx[0], approx[1], cv::Scalar(255, 0, 0), 3, CV_AA);
              cv::line(dst, approx[1], approx[2], cv::Scalar(255, 0, 0), 3, CV_AA);
              cv::line(dst, approx[2], approx[0], cv::Scalar(255, 0, 0), 3, CV_AA);

              break;
          }
          case 4:
          {
              // Cuadrado:

              cv::line(dst, approx[0], approx[1], cv::Scalar(0, 255, 0), 3, CV_AA);
              cv::line(dst, approx[1], approx[2], cv::Scalar(0, 255, 0), 3, CV_AA);
              cv::line(dst, approx[2], approx[3], cv::Scalar(0, 255, 0), 3, CV_AA);
              cv::line(dst, approx[3], approx[0], cv::Scalar(0, 255, 0), 3, CV_AA);

              break;
          }
          case 5:
          {
              // Pentágono:

              cv::line(dst, approx[0], approx[1], cv::Scalar(0, 0, 255), 3, CV_AA);
              cv::line(dst, approx[1], approx[2], cv::Scalar(0, 0, 255), 3, CV_AA);
              cv::line(dst, approx[2], approx[3], cv::Scalar(0, 0, 255), 3, CV_AA);
              cv::line(dst, approx[3], approx[4], cv::Scalar(0, 0, 255), 3, CV_AA);
              cv::line(dst, approx[4], approx[0], cv::Scalar(0, 0, 255), 3, CV_AA);

              break;
          }
          default:
          {
            for(int i = 0; i < approx.size(); i++)
            {
                cv::line(dst, approx[i], approx[i+1], cv::Scalar(255, 0, 255), 3, CV_AA);
            }
          }
      }
}

  cv::imshow("Imagen", thresholdgray);

  cv::imshow("Umbral", edges);

  cv::imshow("Bordes", canny);

  cv::imshow("Solucion", dst);

  char key = cvWaitKey(10);


}

